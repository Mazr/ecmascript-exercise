export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  // eslint-disable-next-line no-param-reassign
  return Object.values(source)
    .map(Number)
    .reduce((num, sum) => (sum += num));
}
